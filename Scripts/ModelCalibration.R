rm(list = ls())

##################################
## 0) Loading packages and set up environment
##################################

library(readxl)
library(deSolve)
library(RColorBrewer)
library(openxlsx)

setwd('..')

##################################
## 1) Defining timing parameters
##################################
## Date of the hospital admission file
date_file <- as.Date('2021-01-11')

## Time window used for the calibration / for the simulations
date_end_calibration <- as.Date('2021-01-04')
date_beginning_calibration <- as.Date('2020-03-15')
date_end_simulation <- date_file + 10

## Timing of the intervention
time_intervention <- 55
date_intervention <- as.Date('2020-03-17')
date_intervention_2 <- as.Date('2020-05-11')
date_intervention_3 <- as.Date('2020-08-01')
date_intervention_4 <- as.Date('2020-10-30')
date_intervention_5 <- as.Date('2020-11-30')

time_intervention_2 <- date_intervention_2 - date_intervention + time_intervention
time_intervention_3 <- date_intervention_3 - date_intervention + time_intervention
time_intervention_4 <- date_intervention_4 - date_intervention + time_intervention
time_intervention_5 <- date_intervention_5 - date_intervention + time_intervention


## Timing at which P[ICU|Hosp] changes
date_begin_change_ICU <- as.Date('2020-03-20')
date_end_change_ICU <- as.Date('2020-04-07')
time_begin_change <- as.numeric(time_intervention + date_begin_change_ICU - date_intervention)
time_end_change <- as.numeric(time_intervention + date_end_change_ICU - date_intervention)

date_begin_second_change_ICU <- as.Date('2020-07-01')
date_end_second_change_ICU <- as.Date('2020-10-01')
time_begin_second_change <- as.numeric(time_intervention + date_begin_second_change_ICU - date_intervention)
time_end_second_change <- as.numeric(time_intervention + date_end_second_change_ICU - date_intervention)

date_begin_third_change_ICU <- as.Date('2020-10-01')
date_end_third_change_ICU <- as.Date('2020-12-01')
time_begin_third_change <- as.numeric(time_intervention + date_begin_third_change_ICU - date_intervention)
time_end_third_change <- as.numeric(time_intervention + date_end_third_change_ICU - date_intervention)

##################################
## 2) Loading data
##################################
### (A) Population data
pop_France_metro <- read.csv2('Data/population_data/PopulationByAge_INSEE_2020.csv', stringsAsFactors = F, header = T)

# Defining age groups
ageMax.ageG <- c(-1, 9, 17, 29, 39, 44, 49, 54, 59, 64, 69, 74, 79, 130)
name.ageG <- c("0-9y", "10-17y", "18-29y", "30-39y", "40-44y","45-49y",
               "50-54y","55-59y", "60-64y","65-69y",  "70-74y", "75-79y",
               "80y+")

n.ageG <- length(name.ageG)

popSize.ageG <- sapply(2:length(ageMax.ageG), FUN = function(tmp_x){
  sum(pop_France_metro[pop_France_metro$age > ageMax.ageG[tmp_x - 1] & pop_France_metro$age <= ageMax.ageG[tmp_x], 'n_tot'])
})
popSize <- sum(popSize.ageG)


### (B) Contact matrices
# Contact matrix before the lockdown
contactMatrix <- readRDS('Data/contact_matrix/mat_regular.rds')
# Contact matrix during the lockdown
contactMatrix.int <- readRDS('Data/contact_matrix/mat_lockdown.rds')
# Contact matrix after the lockdown
contactMatrix.postLockdown <- readRDS('Data/contact_matrix/mat_postlockdown.rds')

colnames(contactMatrix) <- rownames(contactMatrix) <- 
  colnames(contactMatrix.int) <- rownames(contactMatrix.int) <- 
  colnames(contactMatrix.postLockdown) <- rownames(contactMatrix.postLockdown) <- name.ageG

### (C) Hospitalization data
# Loading hospital and ICU admission file
vect_name_region <- c('ARA', 'BFC', 'BRE', 'CVL', 'COR', 'GES', 'HDF', 'IDF', 'NAQ', 'NOR', 'OCC', 'PAC', 'PDL') ## (Metropolitan France)
admission_file <- read.csv(paste0('Data/hospitalisation_data/SIVIC_daily_numbers_region_',
                                  format(date_file, '%Y%m%d'), '.csv'))
admission_file <- admission_file[admission_file$region %in% vect_name_region,]
admission_file$date <- as.Date(admission_file$date, format = '%Y-%m-%d')

# Matrix of daily ICU admissions
mat_daily_ICU <- sapply(vect_name_region, FUN = function(name_region){
  tmp_adm <- admission_file[admission_file$region == name_region,c('date', 'icu_pred')]
  tmp_adm <- tmp_adm[order(tmp_adm$date),]
  tmp_adm[tmp_adm$date >=date_beginning_calibration & tmp_adm$date <= date_end_calibration,]$icu_pred
})

# Matrix of daily hospital admissions
mat_daily_hosp <- sapply(vect_name_region, FUN = function(name_region){
  tmp_hosp <- admission_file[admission_file$region == name_region,c('date', 'hosp_pred')]
  tmp_hosp <- tmp_hosp[order(tmp_hosp$date),]
  tmp_hosp[tmp_hosp$date >=date_beginning_calibration & tmp_hosp$date <= date_end_calibration,]$hosp_pred
})

daily_ICU_admissions <- apply(mat_daily_ICU[,colnames(mat_daily_ICU) %in% vect_name_region], 1, sum)
daily_hosp_admissions <- apply(mat_daily_hosp[,colnames(mat_daily_hosp) %in% vect_name_region], 1, sum)

### (D) Severity estimates
df_severity <- read.csv2('Data/severity_estimates/SeverityEstimatesOurAgeGroups.csv')
p.hosp.inf <- df_severity$IHR
p.icu.hosp.FirstWave <- df_severity$PICU

##################################
## 3) Defining model parameters
##################################
## Time step and vector used when solving the ODE
dt <- 0.25
times_max <- as.numeric(date_end_simulation - date_intervention + time_intervention)
times <- seq(0, times_max, dt)
times_1 <- seq(0, max(times), 1)

## Time to infectiosity (Rate out of E1)
g1 <- 1/4
## Time to symptom onset (Rate out of E2)
g2 <- 1/1
## Time to mild cases recovery
g3 <- 1/3
## Relative contribution of the different trajectories to the infection (used to compute growth rate from R0)
c2 <- g3/(g2+g3)
c3 <- g2/(g2+g3)

## Rate of ICU admissions once hospitalized
g_to_ICU <- 1/1.5

## Rate of hospital admission for patients who will require an ICU admission
time_to_hosp_ICU <- 3
g_to_hosp_ICU <- 1/time_to_hosp_ICU
## Rate of ICU admission for patients who will not require an ICU admission
g_to_hosp <- 1/(time_to_hosp_ICU + 1)

## Age-dependent susceptibility
susceptibility <- c(0.5, 0.75, rep(1, n.ageG - 2))
## Age-dependent infectivity
infectivity <- rep(1.0, n.ageG)

##################################
## 4) Symmetrizing Adjusting the contact matrices to account for relative infectivity of children
##################################
adjust_matrix_infectivity_susceptibility <- function(contactMatrix,
                                                     susceptibility,
                                                     infectivity){
  contactMatrix_corr<- matrix(0, ncol = ncol(contactMatrix), nrow = nrow(contactMatrix))
  for(i in 1:nrow(contactMatrix_corr)){
    for(j in 1:ncol(contactMatrix_corr)){
      contactMatrix_corr[i,j] <- contactMatrix[i,j]*infectivity[j]*susceptibility[i]
    }
  }
  return(contactMatrix_corr)
}

symmetrize_contact_matrix <- function(contactMatrix,
                                      popSize.ageG){
  tmp_mat_pop <- matrix(rep(popSize.ageG, n.ageG), ncol = n.ageG, nrow = n.ageG)
  MatPop <- contactMatrix*tmp_mat_pop
  
  NormalizedMat <- (MatPop + t(MatPop))/(2*tmp_mat_pop)
  return(NormalizedMat)
}


SymmetrizedContactMat <- symmetrize_contact_matrix(contactMatrix, popSize.ageG)
contactMatrix_corr <- adjust_matrix_infectivity_susceptibility(SymmetrizedContactMat, susceptibility, infectivity)
eigenvalues <- eigen(contactMatrix_corr)$values
max_eigenval1 <- max(Re(eigenvalues[abs(Im(eigenvalues)) < 1e-6]))

SymmetrizedContactMat.int <- symmetrize_contact_matrix(contactMatrix.int, popSize.ageG)
contactMatrix_corr.int <- adjust_matrix_infectivity_susceptibility(SymmetrizedContactMat.int, susceptibility, infectivity)
eigenvalues <- eigen(contactMatrix_corr.int)$values
max_eigenval2 <- max(Re(eigenvalues[abs(Im(eigenvalues)) < 1e-6]))

SymmetrizedContactMat.postLockdown <- symmetrize_contact_matrix(contactMatrix.postLockdown, popSize.ageG)
contactMatrix_corr.postLockdown <- adjust_matrix_infectivity_susceptibility(SymmetrizedContactMat.postLockdown, susceptibility, infectivity)
eigenvalues <- eigen(contactMatrix_corr.postLockdown)$values
max_eigenval3 <- max(Re(eigenvalues[abs(Im(eigenvalues)) < 1e-6]))

##################################
## 5) Defining functions used to compute the growthrate
## Ref: J. Wallinga, M. Lipsitch, Proc. Biol. Sci.274, 599-604 (2007)
##################################
f <- function(r, R0){
  res <- (1+r/g1)*(1+r/g2)*(1+r/g3) - c2*(1+r/g3)*R0 - c3*R0
  return(res)
}
get_growth_rate_from_R0 <- function(R0){
  step <- 1e-4
  vect_growth_rates <- seq(0, 0.7, step)
  sup_growth_rate <- vect_growth_rates[which(sapply(vect_growth_rates, FUN = function(x){f(x, R0)}) > 0)[1]]
  growth_rate <- sup_growth_rate - step/2
  return(growth_rate)
}
get_infectious_init <- function(R0, log_Inf_lockdown, time_intervention){
  growth_rate <- get_growth_rate_from_R0(R0)
  log_inf_init <- log_Inf_lockdown - growth_rate*time_intervention
  return(exp(log_inf_init))
}

##################################
## 6) Defining functions to compute to reduction in P[ICU | Hosp]
##################################
# Function used to obtain the correction for a given alpha at a certain time
get_slope <- function(alpha, t){
  tmp_slope <- 1 + (t - time_begin_change)/(time_end_change - time_begin_change)*(alpha - 1)
  return(tmp_slope)
}

## Getting data that were used for first wave estimates
time_beginning_FirstWave <- as.Date('2020-03-01')
time_end_FirstWave <- as.Date('2020-05-07')
date_file_FirstWave <- as.Date('2020-11-04')
national_admission_file <- read.csv(paste0('Data/hospitalisation_data/SIVIC_daily_numbers_',
                                           format(date_file_FirstWave, '%Y%m%d'), '.csv'))
national_admission_file$date <- as.Date(national_admission_file$date, format = '%Y-%m-%d')
vect_time_FirstWave <- as.Date(seq(0, time_end_FirstWave - time_beginning_FirstWave), origin = time_beginning_FirstWave)
vect_hospitalization_FirstWave <- national_admission_file[national_admission_file$date >= time_beginning_FirstWave & national_admission_file$date <= time_end_FirstWave,]$hosp_pred

## Function to compute the baseline probability for a given alpha
## (Baseline = Value of P[ICU|Hosp] before date_begin_change_ICU)
get_baseline_proba <- function(alpha,
                               time_beginning_FirstWave,
                               time_end_FirstWave,
                               vect_hospitalization_FirstWave,
                               proba_FirstWave){
  N_tot <- sum(vect_hospitalization_FirstWave)
  
  N_before_change <- sum(vect_hospitalization_FirstWave[vect_time_FirstWave < date_begin_change_ICU])
  N_after_change <- sum(vect_hospitalization_FirstWave[vect_time_FirstWave > date_end_change_ICU])
  weighted_sum_during_decrease <- sum(sapply(0:(time_end_change - time_begin_change), FUN = function(i){
    tmp_time <- time_begin_change + i
    tmp_date <- date_begin_change_ICU + i
    tmp_slope <- get_slope(alpha, tmp_time)
    tmp_slope*vect_hospitalization_FirstWave[vect_time_FirstWave == tmp_date]
  }))
  
  baseline_proba <- proba_FirstWave*N_tot/(N_before_change + alpha*N_after_change + weighted_sum_during_decrease)
  baseline_proba
}

##################################
## 7) Defining model
##################################
output_from_param <- function(R0,
                              R.lockdown,
                              R.summer,
                              R.rebound,
                              R.second.lockdown,
                              R.after.second.lock,
                              alpha,
                              alpha.2,
                              alpha.3,
                              log_Inf_lockdown){
  
  ## Defining baseline transmission matrix based on the contact matrix we are using
  duree.infectieuse<-1/g2+1/g3
  transRate1 <- R0/duree.infectieuse/max_eigenval1
  transRate2 <- R.lockdown/duree.infectieuse/max_eigenval2
  transRate3 <- R.summer/duree.infectieuse/max_eigenval3
  transRate4 <- R.rebound/duree.infectieuse/max_eigenval3
  transRate5 <- R.second.lockdown/duree.infectieuse/max_eigenval2
  transRate6 <- R.after.second.lock/duree.infectieuse/max_eigenval3
  
  
  B1 <- transRate1*contactMatrix_corr
  B2 <- transRate2*contactMatrix_corr.int
  B3 <- transRate3*contactMatrix_corr.postLockdown
  B4 <- transRate4*contactMatrix_corr.postLockdown
  B5 <- transRate5*contactMatrix_corr.int
  B6 <- transRate6*contactMatrix_corr.postLockdown
  
  
  ## Defining the baseline probability based on first wave estimates
  p.icu.hosp.baseline <- get_baseline_proba(alpha = alpha,
                                            time_beginning_FirstWave = time_beginning_FirstWave,
                                            time_end_FirstWave = time_end_FirstWave,
                                            vect_hospitalization_FirstWave = vect_hospitalization_FirstWave,
                                            proba_FirstWave = p.icu.hosp.FirstWave)
  
  ## Defining the model
  ## Model structure
  SEIR.simple <- function(t,Y, parms){
    with(as.list(c(Y, parms)), {
      S <- Y[1:n.ageG]
      E1 <- Y[1*n.ageG+1:n.ageG]
      E2 <- Y[2*n.ageG+1:n.ageG]
      I.mild <- Y[3*n.ageG+1:n.ageG]
      I.hosp <- Y[4*n.ageG+1:n.ageG]
      I.bar.hosp <- Y[5*n.ageG+1:n.ageG]
      I.bar.ICU <- Y[6*n.ageG+1:n.ageG]
      H.ICU <- Y[7*n.ageG+1:n.ageG]
      ICU.cum <- Y[8*n.ageG+1:n.ageG]
      H.cum <- Y[9*n.ageG+1:n.ageG]
      
      if(t < time_intervention){
        nInfection<-S*(B1%*%((I.mild+I.hosp+E2)/popSize.ageG))
      } else if(t < time_intervention_2){
        nInfection<-S*(B2%*%((I.mild+I.hosp+E2)/popSize.ageG))
      } else if(t < time_intervention_3){
        nInfection<-S*(B3%*%((I.mild+I.hosp+E2)/popSize.ageG))
      } else if(t < time_intervention_4){
        nInfection<-S*(B4%*%((I.mild+I.hosp+E2)/popSize.ageG))
      } else if(t < time_intervention_5){ 
        nInfection<-S*(B5%*%((I.mild+I.hosp+E2)/popSize.ageG))
      } else{
        nInfection<-S*(B6%*%((I.mild+I.hosp+E2)/popSize.ageG))
      }
      
      if(t < time_begin_change){
        p.icu.hosp <- p.icu.hosp.baseline
      } else if(t<= time_end_change){
        slope <- 1 + (t - time_begin_change)/(time_end_change - time_begin_change)*(alpha - 1)
        p.icu.hosp <- slope*p.icu.hosp.baseline
      } else if (t < time_begin_second_change){
        p.icu.hosp <- alpha*p.icu.hosp.baseline
      } else if(t<= time_end_second_change){
        slope <- (alpha.2 - alpha)/(time_end_second_change - time_begin_second_change)*(t - time_begin_second_change) + alpha
        p.icu.hosp <- slope*p.icu.hosp.baseline
      } else if(t < time_begin_third_change){
        p.icu.hosp <- alpha.2*p.icu.hosp.baseline
      } else if(t <= time_end_third_change){
        slope <- (alpha.3 - alpha.2)/(time_end_third_change - time_begin_third_change)*(t - time_begin_third_change) + alpha.2
        p.icu.hosp <- slope*p.icu.hosp.baseline
      } else{
        p.icu.hosp <- alpha.3*p.icu.hosp.baseline
      }
      
      dS<- -nInfection
      dE1<- nInfection - g1*E1
      dE2<- g1*E1-g2*E2
      dI.mild <- g2*(1-p.hosp.inf)*E2 - g3*I.mild
      
      dI.hosp <- g2*p.hosp.inf*E2 - g3*I.hosp
      
      dI.bar.hosp <- g3*(1-p.icu.hosp)*I.hosp - g_to_hosp*I.bar.hosp
      dI.bar.ICU <- g3*p.icu.hosp*I.hosp - g_to_hosp_ICU*I.bar.ICU
      
      dH.ICU <- g_to_hosp_ICU*I.bar.ICU - g_to_ICU*H.ICU
      
      dICU.cum <- g_to_ICU*H.ICU
      dH.cum <- g_to_hosp_ICU*I.bar.ICU + g_to_hosp*I.bar.hosp
      
      
      list(c(dS, dE1, dE2,
             dI.mild, dI.hosp,
             dI.bar.hosp, dI.bar.ICU,
             dH.ICU,
             dICU.cum, dH.cum)) 
    })
  }
  ## Initial conditions
  n_inf_init <- get_infectious_init(R0, log_Inf_lockdown, time_intervention)
  n_inf_init_by_age <- n_inf_init*popSize.ageG/popSize
  
  Yini<-c(popSize.ageG - n_inf_init_by_age, # S      -0
          n_inf_init_by_age, # E1     -1
          rep(0, n.ageG), # E2      -2
          rep(0, n.ageG), # I.mild      -3
          rep(0, n.ageG), # I.hosp      -4
          rep(0, n.ageG), # I.bar.hosp      -5
          rep(0, n.ageG), # I.bar.ICU      -6
          rep(0, n.ageG), # H.ICU      -7
          rep(0, n.ageG), # ICU.cum      -8
          rep(0, n.ageG) # H.cum      -9
  )
  N<-sum(Yini)
  
  ## Running the model
  out <- ode(y = Yini, times = times, func = SEIR.simple, parms = c(time_intervention = time_intervention), atol = 1e-5)
  return(out)
}

# Getting daily ICU admissions from model run
get_daily_ICU_admission_from_output <- function(out){
  ICU.cum <- out[,1+8*n.ageG+1:n.ageG]
  ICU.cum.all <- apply(ICU.cum, 1, sum)
  ICU_adm <- c(0,ICU.cum.all[2:length(ICU.cum.all)] - ICU.cum.all[1:(length(ICU.cum.all) - 1)])
  # Rescale it to daily admissions
  daily_ICU_adm <- sapply(1:length(times_1), FUN = function(i){
    sum(ICU_adm[which(times < times_1[i] + 1 & times >= times_1[i])])
  })
  return(daily_ICU_adm)
}
# Getting daily hospital admissions from model run
get_daily_hosp_admission_from_output <- function(out){
  H.cum <- out[,1+9*n.ageG+1:n.ageG]
  H.cum.all <- apply(H.cum, 1, sum)
  H_adm <- c(0,H.cum.all[2:length(H.cum.all)] - H.cum.all[1:(length(H.cum.all) - 1)])
  # Rescale it to daily admissions
  daily_H_adm <- sapply(1:length(times_1), FUN = function(i){
    sum(H_adm[which(times < times_1[i] + 1 & times >= times_1[i])])
  })
  return(daily_H_adm)
}

##################################
## 8) Functions used for model calibration
##################################
## Negative binomial density
get_neg_binom_density <- function(observed, predicted, delta){
  log_density <- sapply(1:length(observed), FUN = function(i){
    m <- predicted[i]
    k <- observed[i]
    r <- m^delta
    
    res <- lgamma(r + k) - lgamma(k + 1) - lgamma(r)
    
    if(r < 1e-25 & m < 1e-25){
      res <- res - 1e25
      
    } else if(r < 1e-25){
      res <- res + 0.0
    } else if(m<1e-25){
      res <- res - 1e25
    } else{
      res <- res + r*log(r/(r+m))
      res <- res + k*log(m/(r+m))
    }
    res  
    
  })
  
  sum_log_density <- sum(log_density)
  if(sum_log_density < -1e25){
    sum_log_density <- -1e25
  }
  
  return(sum_log_density)
}

## Log-likelihood for a given set of parameters
neg_binom_log_lik_France <- function(R0,
                                     R.lockdown,
                                     R.summer,
                                     R.rebound,
                                     R.second.lockdown,
                                     R.after.second.lock,
                                     alpha,
                                     alpha.2,
                                     alpha.3,
                                     log_Inf_lockdown,
                                     delta){
  
  ## Running model
  out <- output_from_param(R0,
                           R.lockdown,
                           R.summer,
                           R.rebound,
                           R.second.lockdown,
                           R.after.second.lock,
                           alpha,
                           alpha.2,
                           alpha.3,
                           log_Inf_lockdown)
  
  index_begin_calibration <- as.numeric(date_beginning_calibration - date_intervention) + time_intervention
  
  # ICU admissions
  pred_ICU_adm <- get_daily_ICU_admission_from_output(out)[index_begin_calibration:(index_begin_calibration + length(daily_ICU_admissions) - 1)]
  # Hospital admissions
  pred_hosp_adm <- get_daily_hosp_admission_from_output(out)[index_begin_calibration:(index_begin_calibration + length(daily_hosp_admissions) - 1)]
  
  
  LLH <- get_neg_binom_density(observed = daily_ICU_admissions, predicted = pred_ICU_adm, delta = delta) +
    get_neg_binom_density(observed = daily_hosp_admissions, predicted = pred_hosp_adm, delta = delta)
  
  return(LLH)
}

calculateLL_neg_binom_France <- function(param){
  R0 <- param[1]
  R.lockdown <- param[2]
  R.summer <- param[3]
  R.rebound <- param[4]
  R.second.lockdown <- param[5]
  R.after.second.lock <- param[6]
  alpha <- param[7]
  alpha.2 <- param[8]
  alpha.3 <- param[9]
  log_Inf_lockdown <- param[10]
  delta <- param[11]
  
  LLH <- neg_binom_log_lik_France(R0,
                                  R.lockdown,
                                  R.summer,
                                  R.rebound,
                                  R.second.lockdown,
                                  R.after.second.lock,
                                  alpha,
                                  alpha.2,
                                  alpha.3,
                                  log_Inf_lockdown,
                                  delta)
  
  
  return(LLH)
}

##################################
## 9) MCMC run
##################################
## (A) Parametrization of the MCMC
nbIteration <- 10000
seed <- 1
set.seed(seed)

dir_results <- paste0('Results/Model_Calibration')
ifelse(dir.exists(dir_results), '', dir.create(dir_results))
name_res <- paste0(dir_results, '/MCMC_France_nbiter_', nbIteration,'_seed_', seed,'.rds')

run_MCMC <- F

if(run_MCMC){
  t0 <- Sys.time()
  ## (B) MCMC initialization
  # Random draw of inital values of the parameters
  nbParam <- 11
  
  ## Random draw of the parameters
  init_R0 <- runif(n = 1, min = 2, max = 4)
  init_R_lockdown <- runif(n = 1, min = 0.5, max = 1)
  init_R_summer <- runif(n = 1, min = 0.8, max = 1.2)
  init_R_rebound <- runif(n = 1, min = 1, max = 1.5)
  init_R_secondLock <- runif(n = 1, min = 0.5, max = 1.0)
  init_R_postSecondLock <- runif(n = 1, min = 0.5, max = 2.5)
  init_log_inf_lockdown <- runif(n = 1, min = log(1e4), max = log(1e5))
  init_delta <- runif(n = 1, min = 0, max = 1)
  init_alpha <- runif(n = 1, min = 0.5, max = 1)
  init_alpha.2 <- runif(n = 1, min = 0.75, max = 1)
  init_alpha.3 <- runif(n = 1, min = 0.5, max = 1)
  
  param <- c(init_R0,
             init_R_lockdown,
             init_R_summer,
             init_R_rebound,
             init_R_secondLock,
             init_R_postSecondLock,
             init_alpha,
             init_alpha.2,
             init_alpha.3,
             init_log_inf_lockdown,
             init_delta)
  
  ## (C) Defining proposal characteristics
  # Standard deviation of the log-normal proposals
  sdProposal <- c(0.12, #R0
                  0.020, #R_lockdown,
                  0.008, #R.summer
                  0.008, #R.rebound
                  0.035, #R.secondLock
                  0.06, #R.postSecondLock
                  0.25, #alpha
                  0.21, #alpha.2
                  0.20, #alpha.3
                  0.0055, #log_inf
                  0.12) #delta 
  
  ## (D) Defining priors characteristics
  inf_prior <- c(0, 0, 0, 0, 0, 0, 0, 0, 0, log(1e2), 0)
  sup_prior <- c(25, 25, 25, 25, 25, 25, 2, 2, 2, log(1e8), 2)
  
  
  ## (E) Structures to store the results
  # Vector to store logLikelihood
  logLik<-rep(0,nbIteration)
  # Matrix of acceptance/rejection booleans
  accept <- matrix(0, ncol = nbParam, nrow = nbIteration)
  # Matrix to store accepted parameters
  parameters <- matrix(0, ncol = nbParam, nrow = nbIteration)
  
  ## (F) First iteration
  iteration <- 1
  logLik[1] <- calculateLL_neg_binom_France(param)
   parameters[iteration,] <- param
  
  ## (G) Running remaining iterations
  for(iteration in 2:nbIteration){
    if(iteration %%100 == 0){
      print(paste0('Iteration :', iteration))
    }
    
    # Getting parameters and logLik obtained at the previous iteration
    parameters[iteration,] <- parameters[iteration-1,]
    logLik[iteration] <- logLik[iteration-1]
    
    # Parameter updates
    for(iParam in 1:nbParam){
      oldParam <- parameters[iteration - 1,iParam]
      
      ## Sampling a new candidate
      newParam <- oldParam*exp(sdProposal[iParam]*rnorm(1))
      
      if(newParam > sup_prior[iParam] || newParam < inf_prior[iParam]){
        parameters[iteration,iParam]<-oldParam
      } else{
        parameters[iteration,iParam] <- as.numeric(newParam)
        ## Computing the logLik for this new candidate
        newLogLik <- calculateLL_neg_binom_France(parameters[iteration,])
        
        ## Acceptance/Rejection step
        log_acceptance_ratio <- newLogLik-logLik[iteration]+log(newParam)-log(oldParam)
        
        if(log(runif(1))<log_acceptance_ratio){
          logLik[iteration]<-newLogLik
          accept[iteration,iParam]<-1
        } else{
          parameters[iteration,iParam] <- oldParam
        }
      }
    } ## End of update parameters
    
    
  }
  t1 <- Sys.time()
  print(t1 - t0)
  
  ## Getting a list to store the results
  list_res <- list(logLik = logLik,
                   parameters = parameters,
                   accept = accept)
  
  saveRDS(list_res, file = name_res)
}

##################################
## 10) Analyzing MCMC runs
## Functions allow to look at the results of 4 chains with different seeds simultaneously
##################################
BI <- 2000 # Burn-in period
name_res_1 <- paste0(dir_results, '/MCMC_France_nbiter_', nbIteration,'_seed_', 1,'.rds')
name_res_2 <- paste0(dir_results, '/MCMC_France_nbiter_', nbIteration,'_seed_', 2,'.rds')
name_res_3 <- paste0(dir_results, '/MCMC_France_nbiter_', nbIteration,'_seed_', 3,'.rds')
name_res_4 <- paste0(dir_results, '/MCMC_France_nbiter_', nbIteration,'_seed_', 4,'.rds')

list_res_1 <- readRDS(name_res_1)
list_res_2 <- readRDS(name_res_2)
list_res_3 <- readRDS(name_res_3)
list_res_4 <- readRDS(name_res_4)

## Trace plots
plot_chains <- function(list_res_1, list_res_2, list_res_3, list_res_4, BI, title = '', param_names = NULL){
  MyPal <- c('orange2', 'darkslateblue', 'deeppink3', 'cyan3')
  par(mfrow = c(4,3), mar = c(5, 5, 2, 1))
  parameters_1 <- list_res_1$parameters
  parameters_2 <- list_res_2$parameters
  parameters_3 <- list_res_3$parameters
  parameters_4 <- list_res_4$parameters
  
  n_regions <- ncol(parameters_1)
  burnIn <- BI
  ## Convergence of log_inf_regions param
  
  for(parID in 1:ncol(parameters_1)) {
    y_max <- max(c(parameters_1[-(1:burnIn),parID], parameters_2[-(1:burnIn),parID], parameters_3[-(1:burnIn),parID], parameters_4[-(1:burnIn),parID]))
    y_min <- min(c(parameters_1[-(1:burnIn),parID], parameters_2[-(1:burnIn),parID], parameters_3[-(1:burnIn),parID], parameters_4[-(1:burnIn),parID]))
    plot(parameters_1[-(1:burnIn),parID], type = 'l', ylab = param_names[parID],
         xlab = expression(n[iter]), main = param_names[parID],
         ylim = c(y_min, y_max), col = MyPal[1],
         cex.main = 1.5, cex.lab = 1.5, cex.axis= 1.5)
    lines(parameters_2[-(1:burnIn),parID], col = MyPal[2])
    lines(parameters_3[-(1:burnIn),parID], col = MyPal[3])
    lines(parameters_4[-(1:burnIn),parID], col = MyPal[4])
    
  }
  
}

name_param_plot <- c(expression(R['Before 17/03/20']), 
                     expression(R['17/03/20 - 11/05/20']),
                     expression(R['11/05/20 - 01/08/20']),
                     expression(R['01/08/20 - 30/10/20']),
                     expression(R['30/10/20 - 30/11/20']),
                     expression(R['After 30/11/20']),
                     expression(alpha[1]), expression(alpha[2]), expression(alpha[3]),
                     expression(logInf['lockdown']),
                     expression(delta))

plot_chains(list_res, list_res_2, list_res_3, list_res_4, BI, param_names = name_param_plot)

## Acceptance rates

## Parameter estimates

## Running simulation from the posterior distribution
simul_from_posterior <- function(list_res, BI,
                                 n_simul_1, n_simul_2, 
                                 seed = 1){
  
  ## Defining burnIn
  burnIn <- BI
  ## Getting posterior distribution
  parameters <- list_res$parameters
  
  ## Removing BI
  parameters <- parameters[-(1:burnIn),]
  
  ## Seeding the RNG
  set.seed(seed)
  ## Sampling n_simul_1 indices
  index_sample <- sample.int(n = nrow(parameters), size = n_simul_1)
  
  ## Sampling for each region n_simul_1 'baselines' that will be used
  ## as average of the negative binomial
  list_res <- lapply(1:n_simul_1, FUN = function(iIndex){
    index <- index_sample[iIndex]
    tmp_param <- parameters[index,]
    tmp_out <- output_from_param(tmp_param[1], tmp_param[2], tmp_param[3],
                                 tmp_param[4], tmp_param[5], tmp_param[6],
                                 tmp_param[7], tmp_param[8], tmp_param[9],
                                 tmp_param[10])
    tmp_out
  })
  
  
  ## For each of these n_simul_1 baselines, compute daily ICU adm
  pred_daily_ICU_adm <- lapply(1:length(list_res), FUN = function(i){
    tmp_out <- list_res[[i]]
    tmp_ICU_adm <- get_daily_ICU_admission_from_output(tmp_out)
    tmp_delta <- parameters[index_sample[i], 11]
    sapply(tmp_ICU_adm, FUN = function(X){
      if(X == 0){
        rep(0, n_simul_2)
      } else{
        rnbinom(n = n_simul_2, size = X^tmp_delta, mu = X)
      }
    })
  })
  
  pred_daily_hosp_adm <- lapply(1:length(list_res), FUN = function(i){
    tmp_out <- list_res[[i]]
    tmp_ICU_adm <- get_daily_hosp_admission_from_output(tmp_out)
    tmp_delta <- parameters[index_sample[i], 11]
    sapply(tmp_ICU_adm, FUN = function(X){
      if(X == 0){
        rep(0, n_simul_2)
      } else{
        rnbinom(n = n_simul_2, size = X^tmp_delta, mu = X)
      }
    })
  })
  
  list_res_to_save <- list(pred_daily_ICU_adm = pred_daily_ICU_adm,
                           pred_daily_hosp_adm = pred_daily_hosp_adm)
  
  return(list_res_to_save)
}

n_simul_1 <- 100
n_simul_2 <- 100
list_res_simul_France <- simul_from_posterior(list_res_1, BI, n_simul_1, n_simul_2, seed = 1)

## Plotting the fit fit the data
plt_Calibration <- function(pred_daily_ICU_adm, pred_daily_hosp_adm, size_leg){
  Sys.setlocale('LC_TIME', 'English')
  
  pred_daily_ICU_adm_2 <- do.call(rbind, pred_daily_ICU_adm)
  pred_daily_hosp_adm_2 <- do.call(rbind, pred_daily_hosp_adm)
  
  ## Dates for plot
  date_min <- date_intervention - time_intervention
  date_max <- as.Date('2021-01-04')
  seq_date <- seq(1, date_max - date_min + 1)
  index_begin_calibration <- as.numeric(date_beginning_calibration - date_min) + 1
  labels_for_plot <- format(as.Date(seq(0, length(seq_date) - 1), origin = date_min), '%d/%m/%y')
  dates_to_plot <- seq(as.Date('2020-02-01'), as.Date('2021-01-01'), by = 'month')
  labels_dates <- format(dates_to_plot, '%b %y')
  index_labels_dates <- which(labels_for_plot %in% format(dates_to_plot, '%d/%m/%y'))
  index_labels_dates <- c(index_labels_dates)
  
  
  CI_pred_ICU_adm_2 <- sapply(seq_date, FUN = function(i){
    quantile(pred_daily_ICU_adm_2[,i], probs = c(0.5, 0.025, 0.975, 0.25, 0.75))
  })
  CI_pred_hosp_adm_2 <- sapply(seq_date, FUN = function(i){
    quantile(pred_daily_hosp_adm_2[,i], probs = c(0.5, 0.025, 0.975, 0.25, 0.75))
  })
  
  df_simul <- data.frame(date = as.Date(seq(0, length(seq_date) - 1), origin = date_min),
                         ICUAdm = CI_pred_ICU_adm_2['50%',],
                         ICUAdm_Inf = CI_pred_ICU_adm_2['2.5%',],
                         ICUAdm_Sup = CI_pred_ICU_adm_2['97.5%',],
                         HospAdm = CI_pred_hosp_adm_2['50%',],
                         HospAdm_Inf = CI_pred_hosp_adm_2['2.5%',],
                         HospAdm_Sup = CI_pred_hosp_adm_2['97.5%',]
  )
  daily_ICU_admissions
  
  df_simul$ICUAdmObs <- sapply(df_simul$date, FUN = function(tmp_date){
    ifelse(tmp_date >= date_beginning_calibration & tmp_date <= date_end_calibration,
           daily_ICU_admissions[as.numeric(tmp_date - date_beginning_calibration + 1)],
           NA)
  })
  df_simul$HospAdmObs <- sapply(df_simul$date, FUN = function(tmp_date){
    ifelse(tmp_date >= date_beginning_calibration & tmp_date <= date_end_calibration,
           daily_hosp_admissions[as.numeric(tmp_date - date_beginning_calibration + 1)],
           NA)
  })
  
  color_pred <- 'darkslateblue'
  
  PltICUAdm <- ggplot(df_simul, aes(x = date, y = ICUAdm)) +
    geom_line(col = color_pred) +
    theme_classic() +
    scale_x_date(limits = c(as.Date('2020-03-01'), NA),
                 breaks = seq.Date(from = as.Date('2020-03-01'),
                                   to = as.Date('2021-01-01'),
                                   by = 'month'),
                 labels = format(seq.Date(from = as.Date('2020-03-01'),
                                          to = as.Date('2021-01-01'),
                                          by = 'month'), '%b'),
                 name = '',
                 expand = expansion(mult = c(0, 0.05))) + 
    geom_ribbon(aes(ymin= ICUAdm_Inf, ymax = ICUAdm_Sup),
                fill = adjustcolor(color_pred, alpha.f = 0.3)) +
    geom_point(aes(x = date, y = ICUAdmObs)) +
    scale_y_continuous(limits = c(0, NA),
                       name = 'Daily ICU admissions',
                       expand = expansion(mult = c(0, 0.1))) + 
    theme(text = element_text(size = size_leg))
  
  PltHospAdm <- ggplot(df_simul, aes(x = date, y = HospAdm)) +
    geom_line(col = color_pred) +
    theme_classic() +
    scale_x_date(limits = c(as.Date('2020-03-01'), NA),
                 breaks = seq.Date(from = as.Date('2020-03-01'),
                                   to = as.Date('2021-01-01'),
                                   by = 'month'),
                 labels = format(seq.Date(from = as.Date('2020-03-01'),
                                          to = as.Date('2021-01-01'),
                                          by = 'month'), '%b'),
                 name = '',
                 expand = expansion(mult = c(0, 0.05))) + 
    geom_ribbon(aes(ymin= HospAdm_Inf, ymax = HospAdm_Sup),
                fill = adjustcolor(color_pred, alpha.f = 0.3)) +
    geom_point(aes(x = date, y = HospAdmObs)) +
    scale_y_continuous(limits = c(0, NA),
                       name = 'Daily hospital admissions',
                       expand = expansion(mult = c(0, 0.1))) + 
    theme(text = element_text(size = size_leg))
  
  df_for_leg <- data.frame(levels = 1:2)
  
  PltForLeg <- ggplot(df_for_leg, aes(x = levels,
                                      y = levels,
                                      col = as.factor(levels))) + 
    geom_point() +
    scale_color_manual(values = c(color_pred, 'black'),
                       name = "",
                       labels = c('Model calibration',
                                  'Hospitalisation data')) +
    theme_classic() +
    theme(legend.title = element_text(size = size_leg),
          legend.text = element_text(size = size_leg))
  
  
  
  Legend <- get_legend(PltForLeg)
  
  list_PltsCalib <- list(Legend = Legend,
                         PltICUAdm = PltICUAdm,
                         PltHospAdm = PltHospAdm)
  return(list_PltsCalib)
}


plt_Calib <- plt_Calibration(list_res_simul_France$pred_daily_ICU_adm,
                            list_res_simul_France$pred_daily_hosp_adm,
                            size_leg = 16)

PltCalib <- ggarrange(plt_Calib$PltICUAdm,
                      plt_Calib$PltHospAdm,
                      ncol = 2,
                      widths = c(1, 1))

PltCalib <- ggarrange(PltCalib, plt_Calib$Legend,
                      nrow = 2, heights = c(1.5, 0.5))

PltCalib
