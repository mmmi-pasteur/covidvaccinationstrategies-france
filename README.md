# COVIDVaccinationStrategies-France

Data and source code used in Tran Kiem C. et al. A modelling study investigating short and medium-term challenges for COVID-19 vaccination: From prioritisation to the relaxation of measures (2021). [EClinicalMedicine](https://www.thelancet.com/journals/eclinm/article/PIIS2589-5370(21)00281-9/fulltext).

## Data
Data used in this analysis are available in the Data folder. SI-VIC hospitalisation data used for the inference are located in the file **SIVIC_daily_numbers_region_20210111.csv** located i the sub-folder hospitalisation_data. This file has 4 columns indicating:
1. the date
2. the region
3. the number of admissions in hospital (general ward or ICU beds)
4. the number of admissions in ICUs

## Scripts
Prior running any scripts, ensure to set the working directory to the source file location.

**1. Calibration of the model (ModelCalibration.R)**

This provides the scripts:
- to reproduce the calibration of the model describe in the manuscript.
- to run simulation from the estimated posterior distribution.

**2. Running prioritization strategies (RunPrioritisationStrategies.R)**

This provides the scripts:
- to compare vaccination strategies prioritizing vaccines to specific groups defined by their age and comorbidity levels (as in Figure 2).
- to compare different prioritization strategies (as in Figure 3).

**3. Running scenarios with measures' relaxation**

This provides the scripts to evaluate the control measures that remain necessary (defined by the reduction in transmission rates) to avoid reaching a given peak in hospital admissions assuming: (as in Figure 4)
- a given value of the vaccine coverage in individuals older than 65 y.o.
- a given value of the vaccine coverage in individuals aged 18-64 y.o.
- vaccination or not of children
- a value for the basic reproduction number assuming complete relaxation of measures
- a value for the proportion immuned following natural infected
